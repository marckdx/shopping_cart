<?php

session_start();

require("dbconn.php");

if(!isset($_SESSION["codes"]))
{
    $_SESSION["codes"] = array();
}

if(!empty($_POST["quantity"])) {
        $sql = "SELECT * FROM products WHERE code=" . $_POST["code"];
        $result = mysqli_query($dbhandle, $sql);

        $exists_on_cart = false;

        if(mysqli_num_rows($result) >= 1)
        {
            $productByCode = array();
            while($r = mysqli_fetch_assoc($result)) {
                $productByCode[] = $r;
            }

                          
                $itemArray = 
                    array(
                        $productByCode[0]["code"] =>
                            array(
                                'description'=>$productByCode[0]["description"], 
                                'code'=>(int)$productByCode[0]["code"], 
                                'quantity'=> (int) $_POST["quantity"], 
                                'price'=>$productByCode[0]["price"], 
                                'image'=>$productByCode[0]["image"],
                                'type'=>$productByCode[0]["type"],
                                'category'=>$productByCode[0]["category"])
                            );
                
                if(!empty($_SESSION["cart_item"])) {
                    
                    $exists_on_cart = exists_on_array($_POST["code"], $_SESSION["cart_item"]); // in_array($_POST["code"], array_keys($_SESSION["cart_item"]));

                    if($exists_on_cart) {

                        $index = find_index_on_array($_POST["code"], $_SESSION["cart_item"]);
                        if($index > -1)
                        {

                            if(empty($_SESSION["cart_item"][$index]["quantity"])) {
                                $_SESSION["cart_item"][$index]["quantity"] = 0;
                            }

                        
                            $_SESSION["cart_item"][$index]['description'] = $productByCode[0]["description"];
                            $_SESSION["cart_item"][$index]["code"] = ((int)$_POST["code"]);
                            $_SESSION["cart_item"][$index]["quantity"] += ((int)$_POST["quantity"]);
                            $_SESSION["cart_item"][$index]['price'] = $productByCode[0]["price"];
                            $_SESSION["cart_item"][$index]['image'] = $productByCode[0]["image"];
                            $_SESSION["cart_item"][$index]['type'] = $productByCode[0]["type"];
                            $_SESSION["cart_item"][$index]['category'] = $productByCode[0]["category"];
                        }
                        // foreach($_SESSION["cart_item"] as $k => $v) {
                        //         if($productByCode[0]["code"] == $k) {
                        //             if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                        //                 $_SESSION["cart_item"][$k]["quantity"] = 0;
                        //             }
                        //             $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                        //         }
                        // }


                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
            } else {
                $_SESSION["cart_item"] = $itemArray;
            }
        }
        
        $full_quantity = 0;

        foreach($_SESSION["cart_item"] as $item)
        {
            $full_quantity+= $item['quantity'];
        }
        print(
            json_encode(
                array("added" => false, 'itemArray' => $itemArray,'keys' => array_keys($_SESSION["cart_item"]), 'code' => $_POST["code"],'items' => $full_quantity, 'exists_on_cart' => $exists_on_cart, 'count' => count($_SESSION["cart_item"]), 'cart' => $_SESSION["cart_item"])));
	}else{

        $full_quantity = 0;

        foreach($_SESSION["cart_item"] as $item)
        {
            $full_quantity+= $item['quantity'];
        }
        print(
            json_encode(
                array("added" => false, 'items' => $full_quantity, 'exists_on_cart' => null, 'count' => count($_SESSION["cart_item"]), 'cart' => $_SESSION["cart_item"])));
    }


    function exists_on_array($id, $array)
    {
        foreach($array as $item)
        {
            if(isset($item["code"]))
            {
                if((int)$item["code"] === (int)$id){
                    return true;
                }
            }
        }

        return false;
    }

    function find_index_on_array($id, $array)
    {
        $index = -1;
        foreach($array as $item)
        {
            $index++;
            if(isset($item["code"]))
            {
                if((int)$item["code"] === (int)$id){
                    return $index;
                }
            }
                        
        }

        return -1;
    }