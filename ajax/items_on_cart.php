<?php

session_start();

$full_quantity = 0;

foreach($_SESSION["cart_item"] as $item)
{
    $full_quantity+= $item['quantity'];
}

print(
    json_encode(
        array("added" => false, 'items' => $full_quantity, 'count' => count($_SESSION["cart_item"]), 'cart' => $_SESSION["cart_item"])));
