<?php

session_start();
unset($_SESSION["cart_item"]);

print(json_encode(array("added" => false, 'items' => isset($_SESSION["cart_item"]) ? count($_SESSION["cart_item"]) : 0, 
    'cart' => isset($_SESSION["cart_item"]) ? $_SESSION["cart_item"] : array()
)));