<?php

require('dbconn.php');

$sql = "SELECT * FROM products";

if(isset($_GET['search_term']))
{
    $term = $_GET['search_term'];
    $sql = "SELECT * FROM products p WHERE p.description LIKE '%".$term."%'";
}

$result = mysqli_query($dbhandle, $sql);

$rows = array();
   while($r = mysqli_fetch_assoc($result)) {
     $rows[] = $r;
   }

 print json_encode($rows);