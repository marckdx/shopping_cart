<?php
    session_start();

    require("dbconn.php");

  $sql = "SELECT phone_number, first_name, last_name, company_name, email, hash, address, address_plus, country, state, zip_code FROM purchase_order WHERE phone_number=" . $_GET["phone_number"];
  $result = mysqli_query($dbhandle, $sql);

  if(mysqli_num_rows($result) >= 1)
  {
        $purchaseOrder = array();
        while($r = mysqli_fetch_assoc($result)) {
            $purchaseOrder = $r;
            break;
        }

        print(json_encode(array('found' => !empty($purchaseOrder), 'order' => $purchaseOrder )));
  }else{
    print(json_encode(array('found' => false, 'order' => null )));
  }