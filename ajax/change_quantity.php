<?php

session_start();

$code = $_GET["code"];
$quantity = $_GET["quantity"];



if(isset($_SESSION) && isset($_GET["code"]) && isset($_GET["quantity"]))
{
    if($quantity !== "undefined" && $code !== "undefined")
    {
        $index = find_index_on_array($code, $_SESSION["cart_item"]); 
        if($index > -1)
        {
            $_SESSION["cart_item"][$index]["quantity"] = $quantity;
        }
    }
}

$full_quantity = 0;

        foreach($_SESSION["cart_item"] as $item)
        {
            $full_quantity+= $item['quantity'];
        }
        
        print(json_encode(array("added" => false, 'items' => $full_quantity, 'count' => count($_SESSION["cart_item"]), 'cart' => $_SESSION["cart_item"])));


function find_index_on_array($id, $array)
    {
        $index = -1;
        foreach($array as $item)
        {
            $index++;
            if(isset($item["code"]))
            {
                if((int)$item["code"] === (int)$id){
                    return (int)$index;
                }
            }               
        }

        return -1;
    }