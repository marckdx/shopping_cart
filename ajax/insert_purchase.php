<?php

session_start();

require("dbconn.php");

//print_r($_SESSION["cart_item"]);

if(isset($_POST["company_name"]) && isset($_POST["email"]) && isset($_POST["first_name"]) && isset($_POST["last_name"]))
{
    $company_name = $_POST["company_name"];
    $email = $_POST["email"];
    $address = $_POST["address"];
    $address_plus = $_POST["address_plus"];
    $country = $_POST["country"];
    $state = $_POST["state"];
    $zip_code = $_POST["zip_code"];
    $same_address = isset($_POST["same_address"]) ? $_POST["same_address"] : 0;
    $receive_quote_monthly = isset($_POST["receive_quote_monthly"]) ? $_POST["receive_quote_monthly"] : 0;
    $payment_credit_card = isset($_POST["payment_credit_card"]) ? $_POST["payment_credit_card"] : 0;
    $payment_debit_card = isset($_POST["payment_debit_card"]) ? $_POST["payment_debit_card"] : 0;
    $payment_paypal = isset($_POST["payment_paypal"]) ? $_POST["payment_paypal"] : 0;
    $payment_cash = isset($_POST["payment_cash"]) ? $_POST["payment_cash"] : 0;
    $payment_other = isset($_POST["payment_other"]) ? $_POST["payment_other"] : 0;
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $phone_number = $_POST["phone_number"];


    $response = array();
    $hash = md5($email.date('dd/MM/YYYY mm:ss'));

    $sql = "INSERT INTO purchase_order (phone_number, hash, first_name, last_name,company_name, email, address, address_plus, country, state, zip_code, same_address, receive_quote_monthly, payment_credit_card, payment_debit_card, payment_paypal, payment_cash, payment_other) 
            VALUES ('$phone_number','$hash','$first_name', '$last_name','$company_name', '$email', '$address', '$address_plus', '$country', '$state', '$zip_code', $same_address, $receive_quote_monthly, $payment_credit_card, $payment_debit_card, $payment_paypal, $payment_cash, $payment_other);";

    $response['purchase_created'] = mysqli_query($dbhandle, $sql);
    $response['hash'] = $hash;
    
    //$dbhandle->query($sql);
    


    if(!empty($_SESSION["cart_item"]))
    {
        foreach($_SESSION["cart_item"] as $item)
        {
            $product_id = $item["code"];
            $price = $item["price"];
            $quantity = $item["quantity"];
            $name = $item["description"];

            $sql_temp = "INSERT INTO purchase_items (name, price, quantity, product_id, hash) VALUES ('$name', $price, $quantity, $product_id, '$hash');";
            mysqli_query($dbhandle, $sql_temp);
        }
    }

    $dbhandle->close();

}else{
    $response = array('purchase_created' => false);
}

print(json_encode($response));