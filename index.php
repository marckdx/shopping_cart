
<?php 

?>

<html ng-app="appCheckout">
    <head>
        <script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>

        <!-- Bootstrap4 files-->
        <script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
        <link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>

        <!-- Font awesome 5 -->
        <link href="assets/fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">

        <!-- plugin: fancybox  -->
        <script src="assets/plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
        <link href="assets/plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

        <!-- plugin: owl carousel  -->
        <link href="assets/plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="assets/plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
        <script src="assets/plugins/owlcarousel/owl.carousel.min.js"></script>

        <!-- custom style -->
        <link href="assets/css/ui.css" rel="stylesheet" type="text/css"/>
        <link href="assets/css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />

        <!-- custom javascript -->
        <script src="assets/js/script.js" type="text/javascript"></script>

        <title>R.E. Royal Trading Product Center</title>

    </head>

    <body ng-controller="SearchController" class="bg">

    <section class="section-content padding-y-sm">
<div class="container">

<div class="row">
    <div class="col-md-12 py-5 text-center">
        <img src="./images/RERoyalTrading_blck.png" alt="">
        <h2>Buy our products fast and safely </h2>
    </div>  
</div>

<div class="row" ng-init="GetItemsOnCart()">
<div class="col-md-12">
        <form class="py-2" ng-submit="SearchItem()">
			<div class="input-group">
			  <input type="text" class="form-control" ng-model="search_term" placeholder="Search">
			  <div class="input-group-append">
			    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
			  </div>
			</div>
        </form>
</div>
</div>


<div class="card">
	<div class="card-body">
        <div class="row">
            <div class="col-md-3-24"> <strong>Your are here:</strong> </div> <!-- col.// -->
            <nav class="col-md-18-24"> 
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            </ol>  
            </nav> <!-- col.// -->
            <div class="text-right"> 
                <a class="btn btn-primary form-inline" href="#" data-toggle="modal" data-target="#modalItemsOnCart"><i class="fa fa-shopping-cart"></i>&nbsp;{{ count_items_on_cart }}</a>
                <a class="btn btn-{{ count_items_on_cart > 0 ? 'primary' : 'secondary' }} form-inline" href="{{ count_items_on_cart > 0 ? 'checkout.php' : '#' }}" ng-model="button" ng-disabled="count_items_on_cart > 0">Checkout</a>
            </div> <!-- col.// -->
        </div> <!-- row.// -->
	</div> <!-- card-body .// -->
</div>

<div class="row" ng-show="products.length > 0" style="margin-top: 20px;" ng-init="LoadProducts()">
<div class="col-md-3" ng-repeat="product in products">
	<figure class="card card-product">
		<div class="img-wrap"> 
			<img src="assets/images/items/{{ product.image }}">
			<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
		</div>
		<figcaption class="info-wrap">
			<a href="#" class="title">{{ product.description}}</a>
			<div class="action-wrap">
				<button href="#" ng-click="OrderProduct(1, product.code)" class="btn btn-primary btn-sm float-right"> Order </button>
				<div class="price-wrap h5">
					<span class="price-new">${{ product.price}}</span>
					<del class="price-old">${{ product.price}}</del>
				</div> <!-- price-wrap.// -->
			</div> <!-- action-wrap -->
		</figcaption>
	</figure> <!-- card // -->
</div> <!-- col // -->
</div> <!-- row.// -->
</div>
</section>

<div class="modal fade" id="modalItemsOnCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
     
      <div class="modal-body">
                <div class="card">
            <table class="table table-hover shopping-cart-wrap">
            <thead class="text-muted">
            <tr>
                <th scope="col">Product</th>
                <th scope="col" width="120">Quantity</th>
                <th scope="col" width="120">Price</th>
                <th scope="col" width="200" class="text-right">Action</th>
            </tr>
            </thead>
            <tbody>

            <tr ng-repeat="item in items_on_cart"  ng-show="item.quantity > 0 && item.code > 0">
                <td>
            <figure class="media">
                <div class="img-wrap"><img src="assets/images/items/{{ item.image }}" class="img-thumbnail img-sm"></div>
                <figcaption class="media-body">
                    <h6 class="title text-truncate">{{ item.description }}</h6>
                    <dl class="dlist-inline small">
                    <dt>Type: </dt>
                    <dd>{{ item.type }}</dd>
                    </dl>
                    <dl class="dlist-inline small">
                    <dt>Category: </dt>
                    <dd>{{ item.category }}</dd>
                    </dl>
                </figcaption>
            </figure> 
                </td>
                <td> 
                    <input ng-model="item.quantity" readonly="readonly" class="form-control" type="text"/>                    
                   

                    <button class="btn btn-success btn-sm" ng-click="AddItemQuantity(item)"><i class="fa fa-plus"></i></button> 
                    <button class="btn btn-primary btn-sm" ng-click="RemoveItemQuantity(item)"><i class="fa fa-minus"></i></button>
                </td>
                <td> 
                    <div class="price-wrap"> 
                        <var class="price">CAD {{ item.price * item.quantity }}</var> 
                        <small class="text-muted">(CAD {{ item.price }} each)</small> 
                    </div>
                </td>
                <td class="text-right"> 
                <!--<a title="" href="" class="btn btn-outline-success" data-toggle="tooltip" data-original-title="Save to Wishlist"> <i class="fa fa-heart"></i></a>-->
                <a href="#" ng-click="RemoveFromCart(item.code)" class="btn btn-outline-danger"> × Remove</a>
                </td>
            </tr>
            
            </tbody>
            </table>
            <div class="modal-footer">
                <div class="pull-left">
                    <button type="button" class="btn btn-danger" ng-click="ClearCart()">Clear Cart</button>
                </div>
                <div class="pull-right" style="justify-content: flex-end!important;">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
            </div> <!-- card.// -->
      </div>
     
    </div>
  </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script src="app.js" type="text/javascript"></script>

</body>