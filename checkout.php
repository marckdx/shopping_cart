<?php

session_start();

if(isset($_SESSION["cart_item"])){
    $total_quantity = 0;
    $total_price = 0;
}
?>

<!DOCTYPE html>

<html lang="en" slick-uniqueid="3" ng-app="appCheckout"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Re Royal Trading ">
    <meta name="author" content="Alexandre Novaes Iosimura - alexandre@reroyaltrading.ca">
    <link rel="icon" href="https://getbootstrap.com/favicon.ico">

    <title>Re Royal Trading - Whole Sale Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="./index_files/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./index_files/form-validation.css" rel="stylesheet">

<style type="text/css">
@font-face {
  font-weight: 400;
  font-style:  normal;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Regular.woff2') format('woff2');
}
@font-face {
  font-weight: 400;
  font-style:  italic;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Italic.woff2') format('woff2');
}

@font-face {
  font-weight: 500;
  font-style:  normal;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Medium.woff2') format('woff2');
}
@font-face {
  font-weight: 500;
  font-style:  italic;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-MediumItalic.woff2') format('woff2');
}

@font-face {
  font-weight: 700;
  font-style:  normal;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Bold.woff2') format('woff2');
}
@font-face {
  font-weight: 700;
  font-style:  italic;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-BoldItalic.woff2') format('woff2');
}

@font-face {
  font-weight: 900;
  font-style:  normal;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-Black.woff2') format('woff2');
}
@font-face {
  font-weight: 900;
  font-style:  italic;
  font-family: 'Inter-Loom';

  src: url('https://cdn.loom.com/assets/fonts/inter/Inter-UI-BlackItalic.woff2') format('woff2');
}</style></head>

  <body class="bg-light" cz-shortcut-listen="true" ng-controller="CheckoutController">

    <div class="container">
      <div class="py-5 text-center">
        <img  src="./images/RERoyalTrading_blck.png" alt="" >
        <h2>Buy our products fast and safely </h2>
        <p class="lead"></p>
      </div>

      <div class="row">
        <div class="col-md-4 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart</span>
            <span class="badge badge-secondary badge-pill"><?php print(count($_SESSION["cart_item"]));?></span>
          </h4>
          <ul class="list-group mb-3">

          <?php		
          $total = 0;
          foreach ($_SESSION["cart_item"] as $item){
              $item_price = $item["quantity"]*$item["price"];
              $total += $item_price;
		      ?>
            <li class="list-group-item d-flex justify-content-between lh-condensed">
              <div>
                <h6 class="my-0"><?php print($item["description"]); ?></h6>
                <small class="text-muted"><?php print($item["quantity"].' x '. $item["price"]); ?></small>
              </div>
              <span class="text-muted"><?php print($item_price); ?></span>
            </li>
          <?php  } ?>
            <li class="list-group-item d-flex justify-content-between">
              <span>Total (CAD)</span>
              <strong><?php print($total) ?></strong>
            </li>
          </ul>

          <!--<form class="card p-2" ng-submit="SendPromoCode()">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Promo code">
              <div class="input-group-append">
                <button type="submit" class="btn btn-secondary">Redeem</button>
              </div>
            </div>
          </form>-->
        </div>
        <div class="col-md-8 order-md-1">
        <div class="card">
	        <div class="card-body">
          
          <h4 class="mb-3">Billing address</h4>
          <form method="POST" action="ajax/add_to_cart.php">
          <div class="mb-3">
              <label for="phone_number">Phone Number</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" style="color: #ff0000;">*</span>
                </div>
                <input type="text" class="form-control" id="phone_number" ng-blur='FindPhoneNumber()' ng-model="purchase.phone_number" placeholder="Phone Number" required="required">
                <div class="invalid-feedback" style="width: 100%;">
                  Your Name company is required.
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="firstName">First name</label>
                <input type="text" class="form-control"  id="firstName" ng-model="purchase.first_name" placeholder="" value="" required="required">
                <div class="invalid-feedback">
                  Valid first name is required.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Last name</label>
                <input type="text" class="form-control" id="lastName" ng-model="purchase.last_name" placeholder="" value="" required="required">
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
            </div>

            <div class="mb-3">
              <label for="companyname">Name company</label>
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" style="color: #ff0000;">*</span>
                </div>
                <input type="text" class="form-control" id="username" ng-model="purchase.company_name" placeholder="Name Company" required="required">
                <div class="invalid-feedback" style="width: 100%;">
                  Your Name company is required.
                </div>
              </div>
            </div>

            <div class="mb-3">
              <label for="email">Email <span class="text-muted">(Optional)</span></label>
              <input type="email" class="form-control" id="email"  ng-model="purchase.email" placeholder="you@yourcompanydomain.com" required="required">
              <div class="invalid-feedback" style="width: 100%;">
                Your Email is required.
              </div>
            </div>

            <div class="mb-3">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="address" ng-model="purchase.address" placeholder="1234 Main St" required="required">
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="mb-3">
              <label for="address2">Address 2 <span class="text-muted">(Optional)</span></label>
              <input type="text" class="form-control" id="address2" ng-model="purchase.address_plus" placeholder="Apartment or suite">
            </div>

            <div class="row">
              <div class="col-md-5 mb-3">
                <label for="country">Country</label>
                <select class="custom-select d-block w-100" ng-model="purchase.country" id="country" required="">
                  <option value="">Choose...</option>
                  <option>United States</option>
                  <option>Canadá</option>
                  <option>Israel</option>
                </select>
                <div class="invalid-feedback">
                  Please select a valid country.
                </div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="state">State</label>
                <select class="custom-select d-block w-100" ng-model="purchase.state" id="state" required="">
                  <option value="">Choose...</option>
                  <option>California</option>
                  <option>Other</option>
                </select>
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip">Zip Code</label>
                <input type="text" class="form-control" id="zip" ng-model="purchase.zip_code" placeholder="" required="">
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>
            </div>
            <hr class="mb-4">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" ng-true-value="1" ng-false-value="0" class="custom-control-input" id="same-address" ng-model="purchase.same_address">
              <label class="custom-control-label" for="same-address">Shipping address is the same as my billing address</label>
            </div>
            <div class="custom-control custom-checkbox">
              <input type="checkbox" ng-true-value="1" ng-false-value="0" class="custom-control-input" id="save-info" ng-model="purchase.receive_quote_monthly">
              <label class="custom-control-label" for="save-info">I want to receive a quote about other products monthly</label>
            </div>
            <hr class="mb-4">

            <h4 class="mb-3">Payment</h4>

            <div class="d-block my-3">
              <div class="custom-control custom-radio">
                <input id="credit" name="paymentMethod" ng-true-value="1" ng-false-value="0" type="radio" ng-model="purchase.payment_credit_card"  class="custom-control-input" checked="" required="">
                <label class="custom-control-label" for="credit">Credit card</label>
              </div>
              <div class="custom-control custom-radio">
                <input id="debit" name="paymentMethod" ng-true-value="1" ng-false-value="0" type="radio"  ng-model="purchase.payment_debit_card" class="custom-control-input" required="">
                <label class="custom-control-label" for="debit">Debit card</label>
              </div>
              <div class="custom-control custom-radio">
                <input id="paypal" name="paymentMethod" ng-true-value="1" ng-false-value="0" type="radio"  ng-model="purchase.payment_paypal" class="custom-control-input" required="">
                <label class="custom-control-label" for="paypal">PayPal</label>
              </div>
              <div class="custom-control custom-radio">
                <input id="paypal" name="paymentMethod" ng-true-value="1" ng-false-value="0" type="radio"  ng-model="purchase.payment_cash" class="custom-control-input" required="">
                <label class="custom-control-label" for="paypal">Cash</label>
              </div>
              <div class="custom-control custom-radio">
                <input id="paypal" name="paymentMethod" ng-true-value="1" ng-false-value="0" type="radio" class="custom-control-input"  ng-model="purchase.payment_other" required="">
                <label class="custom-control-label" for="paypal">Other</label>
              </div>
            </div>
            
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="button" ng-click="SavePurchase()">Continue to checkout</button>
          </form>
        </div>
        </div>
        </div>
      </div>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">© 2017-2020 Re R.E. Royal Trading</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a target="_blank" href="https://reroyaltrading.ca/">Privacy</a></li>
          <li class="list-inline-item"><a target="_blank" href="https://reroyaltrading.ca/">Terms</a></li>
          <li class="list-inline-item"><a target="_blank" href="https://support.reroyaltrading.ca/">Support</a></li>
        </ul>
      </footer>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./index_files/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="./index_files/popper.min.js"></script>
    <script src="./index_files/bootstrap.min.js"></script>
    <script src="./index_files/holder.min.js"></script>
    
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
    <script src="app.js" type="text/javascript"></script>
  

</body>


</html>