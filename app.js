
var base_url = "http://127.0.0.1/shopping_cart/";

angular.module("appCheckout", []);

angular.module("appCheckout").controller("CheckoutController", function($scope, $http, $window){
    $scope.purchase = {};

    $scope.FindPhoneNumber = function()
    {
        var phone_number = $scope.purchase.phone_number;
        var json_url = base_url + "ajax/find_phone_number.php?phone_number=" + phone_number;
        $http.get(json_url).success(function($data){
            if($data.found)
            {
                $scope.purchase = $data.order;
            }
        });
    }

    $scope.SavePurchase = function()
    {
        var json_url = base_url + "ajax/insert_purchase.php";
        $http({
            method  : 'POST',
            url     : json_url,
            data    :  $.param($scope.purchase), 
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).success(function($data) {
            $scope.SendMail($data.hash);
            if($data.purchase_created)
            {
                //swal("Purchase created!", "Create purchase order", "success");
                window.href = base_url + "view_order.php?hash=" + $data.hash;
            }else{
                swal("Purchase can not be created!", "Create purchase order", "error");
            }
        });
    };

    $scope.SendMail = function($hash)
    {
        var json_url = base_url + "ajax/send_mail.php";
        $scope.purchase.hash = $hash;
        $http({
            method  : 'POST',
            url     : json_url,
            data    :  $.param($scope.purchase), 
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).success(function($data) {
          console.log($data);
        });
    };
});

angular.module("appCheckout").controller("SearchController", function($scope, $http, $window){
    $scope.products = {};
    $scope.search_term = "";

    $scope.count_items_on_cart = 0;
    $scope.items_on_cart = {};    

    $scope.quantities = 
    [
        {"id": 1, "name": 1}, 
        {"id": 2, "name": 2},
        {"id": 3, "name": 3},
        {"id": 4, "name": 4},
        {"id": 5, "name": 5},
        {"id": 6, "name": 6},
        {"id": 7, "name": 7},
        {"id": 8, "name": 8},
        {"id": 9, "name": 9},
        {"id": 10, "name": 10},
        {"id": 11, "name": 11},
        {"id": 12, "name": 12},
        {"id": 13, "name": 13},
        {"id": 14, "name": 14},
        {"id": 15, "name": 15},

    ];

    $scope.AddItemQuantity = function($item)
    {
        $item.quantity = parseInt($item.quantity) + 1;
        $scope.ChangeQuantity($item.code, $item.quantity);
    }

    $scope.RemoveItemQuantity = function($item)
    {
        
        if($item.quantity >= 1)
        {
            $item.quantity = parseInt($item.quantity) - 1;
            $scope.ChangeQuantity($item.code, $item.quantity);
        }else
        {
            $scope.RemoveFromCart($item.code);
        }        
    }

    $scope.SearchItem = function()
    {
        var json_url = base_url + "ajax/search_results.php?search_term=" + $scope.search_term;
        $http.get(json_url).success(function($data){
            $scope.products = $data;
        });
    };

    $scope.GetItemsOnCart = function()
    {
        var json_url = base_url + "ajax/items_on_cart.php";
        $http.get(json_url).success(function($data){
            $scope.count_items_on_cart = $data.items;
            $scope.items_on_cart = $data.cart;
        });
    };

    $scope.ChangeQuantity = function(code, quantity)
    {
        var json_url = base_url + "ajax/change_quantity.php?code=" + code + "&quantity=" + quantity;
        $http.get(json_url).success(function($data){
            $scope.count_items_on_cart = $data.items;
            $scope.items_on_cart = $data.cart;
        });
    }

    $scope.RemoveFromCart = function(code)
    {
        var json_url = base_url + "ajax/remove_from_cart.php?code=" + code;
        $http.get(json_url).success(function($data){
            $scope.count_items_on_cart = $data.items;
            $scope.items_on_cart = $data.cart;
        });
    };

    $scope.LoadProducts = function()
    {
        var json_url = base_url + "ajax/search_results.php";
        $http.get(json_url).success(function($data){
            $scope.products = $data;
        });
    };

    $scope.OrderProduct = function($quantity, $code)
    {
        $scope.temp_product = {};
        $scope.temp_product.quantity = $quantity;
        $scope.temp_product.code = $code;

        var json_url = base_url + "ajax/add_to_cart.php";

        $http({
            method  : 'POST',
            url     : json_url,
            data    : $.param($scope.temp_product), 
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' } 
        }).success(function($data) {
            $scope.count_items_on_cart = $data.items;
            $scope.items_on_cart = $data.cart;
        });
    };

    $scope.ClearCart = function()
    {
        var json_url = base_url + "ajax/clear_cart.php";
        $http.get(json_url).success(function($data){
            $scope.count_items_on_cart = $data.items;
            $scope.items_on_cart = $data.cart;
        });
    };
});