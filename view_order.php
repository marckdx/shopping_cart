<?php 

session_start();

require("ajax/dbconn.php");

?>

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy">

<title>Bootstrap ecommerce UI KIT - Alibaba example html template </title>

<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

<!-- jQuery -->
<script src="assets/js/jquery-2.0.0.min.js" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="assets/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">

<!-- plugin: fancybox  -->
<script src="assets/plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
<link href="assets/plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

<!-- plugin: owl carousel  -->
<link href="assets/plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="assets/plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
<script src="assets/plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- custom style -->
<link href="assets/css/ui.css" rel="stylesheet" type="text/css"/>
<link href="assets/css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />

<!-- custom javascript -->
<script src="assets/js/script.js" type="text/javascript"></script>

<script type="text/javascript">
/// some script

// jquery ready start
$(document).ready(function() {
	// jQuery code

}); 
// jquery end
</script>

</head>
<body>


<section class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
    <table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Addess</th>
            <th>Company Name</th>
        </tr>
    </thead>
    <tbody>

<?php

$sql = "SELECT * FROM purchase_order WHERE hash='" . $_GET["hash"]."'";
$result = mysqli_query($dbhandle, $sql);

while($r = mysqli_fetch_assoc($result)) {
    print("<tr>");
    print("<td>".$r['first_name'].' '.$r['last_name']."</td>");
    print("<td>".$r['email']."</td>");
    print("<td>".$r['address']."</td>");
    print("<td>".$r['company_name']."</td>");
    print("</tr>");
}
?>
</tbody>
</table>
</div>

<div class="table-responsive">
    <table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Addess</th>
            <th>Company Name</th>
        </tr>
    </thead>
    <tbody>
<?php
    $sql = "SELECT po.* FROM purchase_items po JOIN purchase_order pp WHERE pp.hash=po.hash WHERE pp.hash='" . $_GET["hash"]."'";
    $result = mysqli_query($dbhandle, $sql);

    while($r = mysqli_fetch_assoc($result)) {
        print("<tr>");
        print("<td>".$r['name'].' '.$r['last_name']."</td>");
        print("<td>".$r['price']."</td>");
        print("<td>".$r['quantity']."</td>");
        print("<td>".$r['product_id']."</td>");
        print("</tr>");
    }
?>

</section>
</body>
</html>