-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.1.34-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win32
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela royalcheck.products
CREATE TABLE IF NOT EXISTS `products` (
  `code` int(11) NOT NULL AUTO_INCREMENT,
  `active` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `item` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `gross_price` decimal(10,2) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `route` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela royalcheck.products: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
REPLACE INTO `products` (`code`, `active`, `type`, `item`, `description`, `price`, `gross_price`, `category`, `category_id`, `image`, `route`) VALUES
	(1, 1, 'Service', 'Adore Nail Kit', 'Adore Nail Kit', 5.00, 5.65, 'ADORE', 1, '1.jpg', 'adore-nail-kit'),
	(2, 1, 'Service', 'Brokerage Reimb.', 'Brokerage Reimb.', 200.00, 200.00, 'X', 52, '2.jpg', 'brokerage-reimb');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Copiando estrutura para tabela royalcheck.purchase_items
CREATE TABLE IF NOT EXISTS `purchase_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela royalcheck.purchase_items: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `purchase_items` DISABLE KEYS */;
REPLACE INTO `purchase_items` (`id`, `name`, `price`, `quantity`, `product_id`, `hash`) VALUES
	(1, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(2, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(3, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(4, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(5, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(6, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(7, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(8, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(9, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(10, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(11, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(12, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(13, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(14, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(15, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(16, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(17, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(18, 'Adore Nail Kit', 5.00, 10, 1, NULL),
	(19, 'Adore Nail Kit', 5.00, 10, 1, NULL);
/*!40000 ALTER TABLE `purchase_items` ENABLE KEYS */;

-- Copiando estrutura para tabela royalcheck.purchase_order
CREATE TABLE IF NOT EXISTS `purchase_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address_plus` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `same_address` varchar(255) NOT NULL,
  `receive_quote_monthly` varchar(255) NOT NULL,
  `payment_credit_card` int(11) NOT NULL,
  `payment_debit_card` int(11) NOT NULL,
  `payment_paypal` int(11) NOT NULL,
  `payment_cash` int(11) NOT NULL,
  `payment_other` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela royalcheck.purchase_order: ~23 rows (aproximadamente)
/*!40000 ALTER TABLE `purchase_order` DISABLE KEYS */;
REPLACE INTO `purchase_order` (`id`, `first_name`, `last_name`, `company_name`, `email`, `hash`, `address`, `address_plus`, `country`, `state`, `zip_code`, `same_address`, `receive_quote_monthly`, `payment_credit_card`, `payment_debit_card`, `payment_paypal`, `payment_cash`, `payment_other`) VALUES
	(3, 'adasd', 'asdasd', 'asdasdaaaaaaaaaa', 'asdasd@outlook.com', '', 'asdasd', 'asdasd', 'United States', 'California', 'adad', '1', '1', 0, 0, 0, 0, 0),
	(4, 'adasd', 'asdasd', 'asdasdaaaaaaaaaa', 'asdasd@outlook.com', '', 'asdasd', 'asdasd', 'United States', 'California', 'adad', '1', '1', 0, 0, 0, 0, 0),
	(5, 'adasd', 'asdasd', 'asdasdaaaaaaaaaa', 'asdasd@outlook.com', '', 'asdasd', 'asdasd', 'United States', 'California', 'adad', '1', '1', 0, 0, 0, 0, 0),
	(6, 'adasd', 'asdasd', 'asdasdaaaaaaaaaa', 'asdasd@outlook.com', '', 'asdasd', 'asdasd', 'United States', 'California', 'adad', '1', '1', 0, 0, 0, 0, 0),
	(7, 'adasd', 'asdasd', 'asdasdaaaaaaaaaa', 'asdasd@outlook.com', '', 'asdasd', 'asdasd', 'United States', 'California', 'adad', '1', '1', 0, 0, 0, 0, 0),
	(8, 'adasd', 'asdasd', 'asdasdaaaaaaaaaa', 'asdasd@outlook.com', '', 'asdasd', 'asdasd', 'United States', 'California', 'adad', '1', '1', 0, 0, 0, 0, 0),
	(9, 'adasd', 'asdasd', 'asdasdaaaaaaaaaa', 'asdasd@outlook.com', '', 'asdasd', 'asdasd', 'United States', 'California', 'adad', '1', '1', 0, 0, 0, 0, 0),
	(10, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(11, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(12, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(13, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(14, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(15, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(16, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(17, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(18, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(19, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(20, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(21, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(22, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(23, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(24, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0),
	(25, 'marco', 'lima', 'marco', 'marckdx@outlook.com', '3629f5a874aa798a61f9f70ab636960e', 'rua 8', '1', 'United States', 'California', '11', '1', '0', 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `purchase_order` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
